from fastapi import FastAPI, HTTPException
import json
from pydantic import BaseModel




app = FastAPI()




# Leer el archivo JSON
with open('books.json', 'r') as file:
    libros = json.load(file)




class Libro(BaseModel):
    author: str
    country: str
    imageLink: str
    language: str
    link: str
    pages: int
    title: str
    year: int




#Devuelve la lista completa de libros con todo su contenido.




@app.get("/libros")
def lista_libros():
    lista_libros = []
    for libro in libros:
        titulo = libro.get('title')
        autor = libro.get('author')
        anio = libro.get('year')
        idioma = libro.get('language')
        paginas = libro.get('pages')
        pais = libro.get('country')
        imagen = libro.get('imageLink')
        link = libro.get('link')
        lista_libros.append(f"Titulo: {titulo}, Autor: {autor}, Año: {anio}, Idioma: {idioma}, Paginas: {paginas}, Pais: {pais}, Link imagen: {imagen}, Link wikipedia: {link}")
    return lista_libros




#Devuelve la lista completa de libros con sus caracteristicas mas importantes.




@app.get("/libros_resumen")
def lista_resumen():
    libros_resumen = []
    for libro in libros:
        titulo = libro.get('title')
        autor = libro.get('author')
        anio = libro.get('year')
        idioma = libro.get('language')
        libros_resumen.append(f"Titulo: {titulo}, Autor: {autor}, Año: {anio}, Idioma: {idioma}")
    return libros_resumen




#Devuelve los datos del libro seleccionado




@app.get("/libro_elegido")
def obtener_libro(titulo: str = None):
    if not titulo:        
        raise HTTPException(status_code=400, detail="No se ha introducido el nombre de un libro.")
    else:
        libro_elegido = []
        for libro in libros:
            if titulo.lower() == libro["title"].lower():
                for clave, valor in libro.items():
                    libro_elegido.append(f"{clave}: {valor}")
                return libro_elegido
        raise HTTPException(status_code=404, detail="No se ha encontrado el libro seleccionado.")




#Devuelve todos los libros de un autor




@app.get("/libro_autor")
def obtener_libros_autor(autor: str = None):
    if not autor:
        raise HTTPException(status_code=400, detail="No se ha proporcionado el nombre de un autor.")
    libros_autor = []
    for libro in libros:
        if libro["author"].lower() == autor.lower():
            libros_autor.append(libro)
    if len(libros_autor) > 0:
        return libros_autor
    raise HTTPException(status_code=404, detail="El autor seleccionado no tiene libros en esta lista.")




#Agregar un libro a la lista




@app.post("/agregar_libro")
def agregar_libro(libro: Libro = None):
    if not libro:
        raise HTTPException(status_code=400, detail="No se ha introducido el nombre de un libro.")
    for book in libros:
        if book["title"].lower() == libro.title.lower() and book["author"].lower() == libro.author.lower():
            return {"mensaje": f"El libro '{libro.title}' del autor '{libro.author}' ya esta agregado."}
        if len(libros) >= 100:
            raise HTTPException(status_code=400, detail="No puede agregarse el libro ya que la lista no puede superar los 100 libros.")
    nuevo_libro = libro.model_dump()
    libros.append(nuevo_libro)
    with open('books.json', 'w') as file:
       json.dump(libros, file, indent=4)
    return{"mensaje": f"Se agregó correctamente el libro '{libro.title}' del autor '{libro.author}'."}


#Eliminar un libro de la lista


@app.delete('/eliminar_libro')
def eliminar_libro(titulo: str = None):
    if not titulo:        
        raise HTTPException(status_code=400, detail="No se ha introducido el nombre de un libro.")
    for libro in libros:
        if titulo.lower() == libro["title"].lower():
            libros.remove(libro)
            with open('books.json', 'w') as file:
                json.dump(libros, file, indent=4)
            return{"mensaje": f"Se eliminó correctamente el libro '{titulo}'."}
    raise HTTPException(status_code=404, detail="No se ha encontrado el libro seleccionado.")


#Editar un libro de la lista


@app.put('/editar_libro')
def put_libro(titulo: str = None, libro_editado: Libro = None):
    for libro in libros:
        if titulo.lower() == libro["title"].lower():
            for clave, valor in libro_editado.model_dump().items():
                if valor != "" and valor != 0:
                    libro[clave]=valor
            with open('books.json', 'w') as f:
                json.dump(libros, f, indent=4)
            return {"El libro ha sido modificado exitosamente"}
    raise HTTPException(status_code=404, detail = "No es posible actualizar el libro. No existe ningún libro con el nombre ingresado")