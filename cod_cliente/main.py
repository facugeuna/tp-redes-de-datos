import requests




url = "http://192.168.1.111:8000"




def obtener_lista_libros():
    response = requests.get(f"{url}/libros")
    if response.status_code == 200:
        libros = response.json()
        for libro in libros:
            print(libro)




def obtener_lista_resumen():
    response = requests.get(f"{url}/libros_resumen")
    if response.status_code == 200:
        libros = response.json()
        for libro in libros:
            print(libro)




def obtener_libro(titulo):
    response = requests.get(f"{url}/libro_elegido", params={"titulo": titulo})
    if response.status_code == 200:
        libro = response.json()
        print(f"Libro encontrado: {libro}")
    else:
        print(response.status_code, response.json())




def obtener_libros_autor(autor):
    response = requests.get(f"{url}/libro_autor", params={"autor": autor})
    if response.status_code == 200:
        libros = response.json()
        for libro in libros:
            print(libro)
    else:
        print(response.status_code, response.json())




def agregar_libro():
    print("Ingrese los datos necesarios para agregar un nuevo libro.")
    libro_nuevo = {
        "author": input("Autor: ").lower(),
        "country": input("Pais: ").lower(),
        "imageLink": input("Link imagen: "),
        "language": input("Idioma: ").lower(),
        "link": input("Link wikipedia: "),
        "pages": int(input("Paginas: ")),
        "title": input("Titulo: ").lower(),
        "year": int(input("Año: "))
     }
   
    response = requests.post(f"{url}/agregar_libro", json = libro_nuevo)




    if response.status_code == 200:
        print("Se agrego correctamente el libro.")
    else:
        print(response.status_code, response.json())


def eliminar_libro_cliente(titulo):
    response = requests.delete(f"{url}/eliminar_libro", params={"titulo": titulo})
    if response.status_code == 200:
        print("Se eliminó correctamente el libro.")
    else:
        print(response.status_code, response.json())


def check_int(valor):
    try:
        int(valor)
        return int(valor)
    except ValueError:
        return 0
   
def editar_libro_cliente(titulo):
    print("Ingrese los datos necesarios para editar un libro.")
    libro_editado = {
        "author": input("Autor: ").lower(),
        "country": input("Pais: ").lower(),
        "imageLink": input("Link imagen: "),
        "language": input("Idioma: ").lower(),
        "link": input("Link wikipedia: "),
        "title": input("Titulo: ").lower(),
        "pages": check_int(input("Paginas: ")),
        "year": check_int(input("Año: "))
     }  
    response = requests.put(f"{url}/editar_libro",params={"titulo": titulo},json = libro_editado)


    if response.status_code == 200:
        print("Se editó correctamente el libro.")
    else:
        print(response.status_code, response.json())


def menu_principal():
    print("Menú:")
    print("1. Mostrar todos los libros disponibles en la lista.")
    print("2. Mostrar todos los libros disponibles en la lista solo con sus caracteristicas principales.")
    print("3. Obtener los datos de un libro por su titulo.")
    print("4. Visualizar los libros por autor.")
    print("5. Agregar un libro.")
    print("6. Actualizar un libro.")
    print("7. Borrar un libro.")
    print("8. Salir")




while True:
    menu_principal()
    opcion_elegida = input("Ingrese una opcion por favor: ")
    try:
        opcion_elegida = int(opcion_elegida)
        if opcion_elegida == 1:
            libros = obtener_lista_libros()
            print("Lista de libros: ")
            print(libros)
        elif opcion_elegida == 2:
            libros = obtener_lista_resumen()
            print("Lista de libros: ")
            print(libros)
        elif opcion_elegida == 3:
            titulo = input("Ingrese el nombre del libro a elegir: ")
            obtener_libro(titulo)
        elif opcion_elegida == 4:  
            autor = input("Ingrese el nombre del autor: ")
            obtener_libros_autor(autor)
        elif opcion_elegida == 5:
            print("Agregará un libro a la lista.")
            agregar_libro()
        elif opcion_elegida == 6:
            print("Editará un libro de la lista.")
            titulo = input("Ingrese el nombre del libro a editar: ")
            editar_libro_cliente(titulo)
        elif opcion_elegida == 7:
            titulo = input("Ingrese el nombre del libro a eliminar: ")
            eliminar_libro_cliente(titulo)
        elif opcion_elegida == 8:
            break
        else:
            print("Opcion incorrecta. Vuelva a indicar una opcion.")
    except ValueError:
        print("Entrada no válida, por favor ingrese un número.")